
	rccd-c-client

WHAT IS rccd-c-client?

  rccd stands for Raspberry Pi Camera daemon.

  This client intends to be a simple client to send a Raspberry Pi
  camera command to a Raspberry Pi running rccd daemon and retrieve
  a picture taken by the Raspberry Pi using that command.

  To launch the client:

    rccd-client 192.168.1.201 raspistill -e jpg -o /tmp/test.jpg

  Where 192.168.1.201 is the Raspberry Pi running rccd and the rest
  of arguments are the command we want to run.

  In the client the picture will be stored in the same path.  

  There is no authentication of the client and I am pretty sure that
  there are a lot of improvements that you could do it ;-).

ON WHAT HARDWARE DOES IT RUN?

  This version is able to run on whatever hardware in which an ANSI C
  compiler could be used.
_______________________________________________________________________
 (c) 2013 ache4bits <ache4bits@gmail.com> 
     Distributed under GNU GPL v2 License                   
     See COPYING.txt for more details                       
