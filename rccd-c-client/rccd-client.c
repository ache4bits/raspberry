/* (c) 2013 ache4bits <ache4bits@gmail.com> */
/*     Distributed under GNU GPL v2 License                   */
/*     See COPYING.txt for more details                       */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define PORT 31415       
#define BUFFER_SIZE 1024

/* arguments have to be the raspberry pi address and the command we want to execute on raspberry pi: */
/*           rccd-client 192.168.1.201 raspistill -e jpg -o /tmp/test.jpg                            */
/*           first argument: raspberry pi ip address where rccd is listening on port 31415           */
/*           the following arguments are the command we want to execute on the raspberry pi          */
/*                                                                                                   */

int main(int argc, char *argv[]) {
    int sockfd, len = 0, res, i;

    char *cmd, *buf_in;

    FILE *fd;

    /* server address */
    struct sockaddr_in srvr_addr; 

    /* checking number of arguments */
    if ( argc <= 3 ) { /* begin - if */
      printf("Insuficient number of arguments.\n");
      exit(1);
    } /* end - if */

    /* opening file to store image */
    fd=fopen(argv[argc - 1],"wb");

    if ( fd == NULL ) { /* begin - if */
      printf("File %s could not be opened.\n", argv[argc - 1]);
      exit(1);
    } /* end - if */

    /* command to send to rccd at raspberry */
    for(i=2;i<argc;i++) { /* begin - for */
      ++len;
      len += strlen(argv[i]);
    } /* end - for */
    cmd = (char *)malloc(len*sizeof(char));
    if ( cmd == NULL ) {
      printf("Error with memory reservation.\n");
      exit(1);
    }

    strcat(cmd,argv[2]);
    len = strlen(argv[2]);
    *(cmd+len) = ' ';

    for(i=3; i<argc; i++) { /* begin - for */
        strcat(cmd+len, argv[i]);
        len += strlen(argv[i])+1;
        *(cmd+len) = ' ';
    } /* end - for */
    *(cmd-1) = 0;
    
    /* allocating buffer memory */
    buf_in = (char *)malloc(BUFFER_SIZE*sizeof(char)); 

    /* socket configuration */
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("socket");
      exit(1);
    }
    
    srvr_addr.sin_family = AF_INET;
    srvr_addr.sin_port = htons(PORT);
    srvr_addr.sin_addr.s_addr = inet_addr(argv[1]);

    /* zeroed struct */
    memset(&(srvr_addr.sin_zero),'\0', 8);
    if (connect(sockfd, (struct sockaddr *)&srvr_addr, sizeof(struct sockaddr)) == -1) { /* begin - if */
      perror("connect");
      exit(1);
    } /* end - if */
    
    /* sending command to server */
    send(sockfd, cmd, 1024*sizeof(char),0);

    /* zeroing input buffer */
    memset(buf_in, '\0', BUFFER_SIZE*sizeof(char));

    /* getting file from rccd server */
    while ( (res = recv(sockfd, buf_in, BUFFER_SIZE*sizeof(char),0)) > 0 ) { /* begin - while */
      res = fwrite(buf_in, sizeof(char), res, fd);
      memset(buf_in, '\0', BUFFER_SIZE*sizeof(char)); /* buffer cleaning */
      fflush(fd);
    } /* end - while */

    /* closing file */
    fclose(fd);	    

    /* closing socket */	
    close(sockfd);

    printf("Image was stored to %s.\n", argv[argc - 1]);

    return 0;
}

