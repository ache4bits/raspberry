/* (c) 2013 ache4bits <ache4bits@gmail.com> */
/*     Distributed under GNU GPL v2 License                   */
/*     See COPYING.txt for more details                       */

#ifndef FINISH_H
  #define FINISH_H
  void finish(char *filename, char *msglog);
#endif
