/* (c) 2013 ache4bits <ache4bits@gmail.com> */
/*     Distributed under GNU GPL v2 License                   */
/*     See COPYING.txt for more details                       */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <syslog.h>
#include <arpa/inet.h>

#include "finish.h"

#define TRUE 1  
#define PORT 31415
#define MAX_CLIENTS 1
#define CMD_LENGTH 1024
#define BUFFER_SIZE 10240

extern char pidfile[];

void rccd (void) { /* begin - rccd function */

  int socketfd, /* socket */
      i = 0;

  struct sockaddr_in server; /* server addr information */

  char cltadd[INET_ADDRSTRLEN],
       cmdcheck[] = "raspistill";

  int msgsock,
    msgsize,
    cmdstatus,
    cmdlength;

  char buf_in[CMD_LENGTH], /* input buffer */
    *filename;

  socketfd = socket(AF_INET, SOCK_STREAM, 0); /* socket creation */

  if( socketfd < 0 ) {  /* begin - if */
    finish(pidfile, "Error opening the socket.\n");
    exit(1);
  } /* end - if */

  server.sin_family=AF_INET;
  server.sin_port=htons(PORT);    
  server.sin_addr.s_addr=INADDR_ANY;
  memset(&(server.sin_zero), '\0', 8);
    
  if( bind(socketfd, (struct sockaddr *)&server,sizeof(server)) ) { /* begin - if */
    finish(pidfile, "Error binding the socket.\n");
    exit(1);
  } /* end - if */

  /* listen to MAX_CLIENTS */ 
  listen(socketfd, MAX_CLIENTS);

  do { /* begin - do while */
    msgsock = accept(socketfd, 0, 0);

    /* cleaning input buffer */
    memset(buf_in, '\0', sizeof(buf_in));

    if( msgsock == -1 ) { /* begin - if */
      syslog(LOG_LOCAL3, "Error accepting connection.\n");
    } /* end - if */
    else { /* begin - else */
      msgsize=recv(msgsock, buf_in, CMD_LENGTH*sizeof(char), 0); /* reading buffer */
      if ( msgsize == -1 ) { /* begin - if */
	syslog(LOG_LOCAL3, "Error reading the buffer.\n");
	continue;
      } /* end - if */

      /* checking if the command is a raspistill command to avoid to execute dangerous commands */
      for(i=0;i<sizeof(cmdcheck)-1;i++) { /* begin - for */
        if ( cmdcheck[i] != buf_in[i] ) { /* begin - if */
          syslog(LOG_LOCAL3, "The recieved command is not a raspisill command: %s.", buf_in);
          break;
        } /* end - if */
      } /* end - for */
      
      if ( i < sizeof(cmdcheck) - 1 ) { /* begin - if */
        close(msgsock);
        syslog(LOG_LOCAL3, "Server closed client connection due to the recieved command is not valid.");
        continue;
      } /* end - if */

      /* getting filename */
      cmdlength = (int)(sizeof(buf_in)/sizeof(char));
      for(i=0;i<cmdlength;i++) { /* begin - for */
        if ( buf_in[i] == '-' && buf_in[i+1] == 'o' && buf_in[i+2] == ' ' ) { /* begin - if */
          filename = (char *)malloc((cmdlength-(i+3))*sizeof(char));
          if ( filename == NULL ) { /* begin - if */
            finish(pidfile, "Problem with memory reservation. Exiting.\n");
          } /* end - if */
          strcpy(filename,buf_in + i + 3);
          break;
        } /* end - if */
      } /* end - for */  

      /* insert \0 in first blank space to get rid of the rest of parameters in case filename is not
         the last parameter */
      for(i=0;filename[i]!='\0';++i) { /* begin - for */
        if ( filename[i] == ' ' ) { /* begin - if */
          filename[i] = '\0';
          break;
        } /* end - if */
      } /* end - for */

      /* executing cmd */
      cmdstatus = system(buf_in);
      if ( cmdstatus != 0 ) {  /* begin - if */
      	syslog(LOG_LOCAL3, "Error using raspberry pi camera with command: %s.\n", buf_in);
        continue;
      }  /* end - if */
      syslog(LOG_LOCAL3, "Executed: %s\n", buf_in);

      /* sending picture to client */
      cmdstatus = sending_file_to_client(filename, msgsock);
      sleep(3);
      close(msgsock);
      syslog(LOG_LOCAL3, "Server closed client connection.\n");
    } /* end - else */
  } while (1); /* end - do while */

} /* end - rccd function */

/* function to send a file to a client */

int sending_file_to_client(char *filename, int msgsock) { /* begin - sending_file_to_client function */

  int res = 1;

  FILE *filefd;

  char *filedata;

  filefd = fopen(filename, "rb");

  if ( filefd == NULL ) { /* begin - if*/
    openlog("rccd daemon",LOG_NOWAIT|LOG_PID,LOG_DAEMON);
    syslog(LOG_LOCAL3, "Error opening image/video file: %s.\n", filename);
    return -1;
  } /* end - if */

  /* allocating memory */
  filedata = (char *)malloc(sizeof(char)*BUFFER_SIZE);
  if ( filedata == NULL ) { /* begin - if */
    openlog("rccd daemon",LOG_NOWAIT|LOG_PID,LOG_DAEMON);
    syslog(LOG_LOCAL3, "Error allocating memory (BUFFER_SIZE elements).\n");
    return -1;
  } /* end - if */

  /* send info to client */
  syslog(LOG_LOCAL3, "Sending image to client.\n");
  while ( !feof(filefd) ) { /* begin - while */
    memset(filedata, '\0', BUFFER_SIZE*sizeof(char));
    res = fread(filedata, sizeof(char), BUFFER_SIZE, filefd);
    res = send(msgsock, filedata, res*sizeof(char), 0);
    if ( res == -1 ) { /* begin - if */
      openlog("rccd daemon",LOG_NOWAIT|LOG_PID,LOG_DAEMON);
      syslog(LOG_LOCAL3, "Error writing to socket.\n");
      fclose(filefd);
      free(filedata);
      return res;
    } /* end - if */ 
  } /* end - while */
  syslog(LOG_LOCAL3, "Image sent to client.\n");
  /* freeing resources */
  fclose(filefd);
  free(filedata);

  return res;

} /* end - sending_file_to_client function */
