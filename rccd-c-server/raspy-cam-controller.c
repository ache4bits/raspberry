/* 
   raspy-cam-controller daemon or rccd
 */

/* (c) 2013 ache4bits <ache4bits@gmail.com> */
/*     Distributed under GNU GPL v2 License                   */
/*     See COPYING.txt for more details                       */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <syslog.h>
#include <signal.h>

#include "rccd.h"
#include "mysignal.h"

char pidfile[] = "/var/run/rccd.pid";

int main(int argc, char* argv[]) { /* begin - main function */

  pid_t pid = 0;
  pid_t sid = 0;

  FILE *pidfd;

  /* openning a connection to the syslog server */
  openlog("rccd daemon",LOG_NOWAIT|LOG_PID,LOG_DAEMON);
  
  /* intercepting signals to log exit */
  signal(SIGKILL, signal_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGPIPE, signal_handler);

  pid = fork(); /* child process creation */

  if ( pid < 0 ) { /* begin - if */
    syslog(LOG_LOCAL3, "Fork process failed.\n");
    exit(1);
  } /* end - if */

  /* killing father process */
  if ( pid > 0 ) { /* begin - if */
    exit(0);
  } /* end - if */

  /* unmasking the file mode */
  umask(0);

  /* setting new session id */
  sid = setsid();

  if( sid < 0 ) { /* begin - if */
    syslog(LOG_LOCAL3, "Fork process failed.\n");
    exit(1);
  } /* end - if */

  /* writing pid to a file */
  pidfd = fopen(pidfile, "w");
  if ( pidfd == NULL ) { /* begin - if */
    syslog(LOG_LOCAL3, "File %s could not be opened. Exiting\n", pidfile);
    exit(1);
  } /* end - if */
  fprintf(pidfd, "%d", sid);
  fclose(pidfd);

  /* setting current working directory to root */
  chdir("/");

  /* closing stdin. stdout and stderr */
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  syslog(LOG_LOCAL3, "rccd started.\n");

  while (1) { /* begin - while */
    sleep(1);
    rccd();
  } /* end - while */

  return (0);
} /* end - main function */
