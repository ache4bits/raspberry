/* (c) 2013 ache4bits <ache4bits@gmail.com> */
/*     Distributed under GNU GPL v2 License                   */
/*     See COPYING.txt for more details                       */

#include <syslog.h>
#include <unistd.h>

/* function to be executed before planned ending */

void finish(char *filename, char *msglog) {

  openlog("rccd daemon",LOG_NOWAIT|LOG_PID,LOG_DAEMON);
  syslog(LOG_LOCAL3, msglog);
  unlink(filename);
  
  return;
}
