/* (c) 2013 ache4bits <ache4bits@gmail.com> */
/*     Distributed under GNU GPL v2 License                   */
/*     See COPYING.txt for more details                       */

#ifndef RCCD_H
  #define RCCD_H
  void rccd (void);
  int sending_file_to_client(char *filename, int msgsock);
#endif
